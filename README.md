# Recipy App API Proxy

NGINX proxy app for our recipy app API

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app which the proxy will forward requests to (default: `9000`)

> ***INFO***: this is a project that is followed along with [this UDEMY course](https://www.udemy.com/share/1031cK3@fWRNZvypKPbqmHO7cNvdseCMhb8XjOO9GTYLkZLK0-LerAG1FSZyRpMLnpjZWu0a/)``